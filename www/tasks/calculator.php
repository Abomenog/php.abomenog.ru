<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/calculator.css">
    <title>Calculator</title>
</head>
<body>

    <h1>Прототип калькулятора:</h1>

    <form action="" method="post" class="calculator_form">
        <input type="text" name="number_1" class="numbers" placeholder="Первое число">
        
        <select class="operations" name="operation">
            <option value="plus"> + </option>
            <option value="minus"> - </option>
            <option value="multiply"> x </option>
            <option value="divide"> / </option>
        </select>

        <input type="text" name="number_2" class="numbers" placeholder="Второе число">
        </br>   
        <input class="submit_form" type="submit" name="submit" value="Получить ответ">

    </form>

<?php
    $operation = 0;
    if(isset($_POST['submit'])) {

        $number_1 = $_POST['number_1'];
        $number_2 = $_POST['number_2'];
        $operation = $_POST['operation'];
    }

    if(!$operation || (!$number_1 && $number_1 != '0') || (!$number_2 && !$number_2 != '0')) {
        $error_result = 'Не все поля заполнены!';
    }

    else {
        if(!is_numeric($number_1) || !is_numeric($number_2)) {
            $error_result = 'Операнды должны быть числами!';
        }

        else 
            switch($operation) {
                case 'plus':
                    $result = $number_1 + $number_2;
                    break;
                case 'minus':
                    $result = $number_1 - $number_2;
                    break;
                case 'multyply':
                    $result = $number_1 * $number_2;
                    break;
                case 'divide':
                    if($number_2 == '0')
                    $error_result = 'На ноль делить нельзя!';
                    else 
                        $result = $number_1 / $number_2;
                        break;
            }
    }

    if(isset($error_result)) {
        echo 'Ошибка : '.$error_result;
    }
    else 
        echo 'Ответ : '.$result;
?>
</body>
</html>